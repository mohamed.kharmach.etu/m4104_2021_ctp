package fr.ulille.iutinfo.teletp;

import android.net.sip.SipSession;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {


    String salle, poste, DISTANCIEL;

    private SuiviViewModel suivi;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DISTANCIEL = getActivity().getResources().getStringArray(R.array.list_salles)[0];
        poste = "";
        salle  = DISTANCIEL;
        suivi = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);

        Spinner spSalle = (Spinner) getActivity().findViewById(R.id.spSalle);
        Spinner spPoste = (Spinner) getActivity().findViewById(R.id.spPoste);

        ArrayAdapter<CharSequence> adapterSpSalle = ArrayAdapter.createFromResource(getContext(), R.array.list_salles, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> adapterSpPoste = ArrayAdapter.createFromResource(getContext(), R.array.list_postes, android.R.layout.simple_spinner_item);

        adapterSpSalle.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterSpPoste.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spSalle.setAdapter(adapterSpSalle);
        spPoste.setAdapter(adapterSpPoste);

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            TextView tvLogin = getActivity().findViewById(R.id.tvLogin);
            Toast log = Toast.makeText(getActivity(), tvLogin.getText(), Toast.LENGTH_LONG);
            log.show();
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        spPoste.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                poste = spPoste.getSelectedItem().toString();
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spSalle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                salle = spSalle.getSelectedItem().toString();
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        update();

        TextView tvNextQuestion = getActivity().findViewById(R.id.tvNextQuestion);
        tvNextQuestion.setVisibility(View.VISIBLE);

        final Observer<Integer> nextQuestionObserver = new Observer<Integer>() {
            @Override
            public void onChanged(Integer i) {
                tvNextQuestion.setText(suivi.getQuestions(i));
            }
        };
        suivi.getLiveNextQuestion().observe(getActivity(), nextQuestionObserver);
    }

    private void update() {
        Spinner spPoste = (Spinner) getActivity().findViewById(R.id.spPoste);
        if (salle.equals(DISTANCIEL)) {
            spPoste.setVisibility(View.INVISIBLE);
            spPoste.setEnabled(false);
            suivi.setLocalisation("Distanciel");
        } else {
            spPoste.setVisibility(View.VISIBLE);
            spPoste.setEnabled(true);
            suivi.setLocalisation(salle + poste);
        }
    }
    // TODO Q9
}